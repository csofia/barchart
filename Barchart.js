datasetTotal = [
    { label: "User", value: 360 },
    { label: "Device", value: 226 },
    { label: "Network", value: 100 },
    { label: "App", value: 120 },
    { label: "Data", value: 130 },
];
datasetOption1 = [
    { label: "User", value: 297 },
    { label: "Device", value: 200 },
    { label: "Network", value: 188 },
    { label: "App", value: 150 },
    { label: "Data", value: 187 },
];
datasetOption2 = [
    { label: "User", value: 220 },
    { label: "Device", value: 257 },
    { label: "Network", value: 278 },
    { label: "App", value: 200 },
    { label: "Data", value: 187 },
];
// var color = d3.scale.ordinal().range(["#6B486B", "#A05D56", "#D0743C", "#FF8C00"]);
d3.selectAll("input").on("change", selectDataset);
function selectDataset() {
    var value = this.value;
    if (value == "total") {
        change(datasetTotal);
    } else if (value == "option1") {
        change(datasetOption1);
    } else if (value == "option2") {
        change(datasetOption2);
    }
}
var margin = {
    top: parseInt(d3.select("body").style("height"), 10) / 30,
    right: parseInt(d3.select("body").style("width"), 10) / 30,
    bottom: parseInt(d3.select("body").style("height"), 10) / 30,
    left: parseInt(d3.select("body").style("width"), 10) / 20,
},
    width =
        parseInt(d3.select("body").style("width"), 10) -
        margin.left -
        margin.right,
    height =
        parseInt(d3.select("body").style("height"), 10) -
        margin.top -
        margin.bottom;
var div = d3.select("body").append("div").attr("class", "toolTip");
var formatPercent = d3.format("");
var y = d3.scale.ordinal().rangeRoundBands([height, 0], 0.2, 0.5);
var x = d3.scale
    .linear()
    .domain([d3.max(dataset)])
    .range([0, width]);
var xAxis = d3.svg.axis().scale(x).tickSize(-height).orient("bottom");
var yAxis = d3.svg.axis().scale(y).orient("left");
//.tickFormat(formatPercent);
//Adding color to the Bars
/* function colorPicker(value) {
        if (value<= 200) {
            return "blue";
        } else if (value> 200) {
            return "red";
        }
    } */
var svg = d3
    .select("body")
    .append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
svg
    .append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis);
d3.select('input[value="total"]').property("checked", true);
change(datasetTotal);
function change(dataset) {
    y.domain(
        dataset.map(function (d) {
            return d.label;
        })
    );
    x.domain([
        0,
        d3.max(dataset, function (d) {
            return d.value;
        }),
    ]);
    svg
        .append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);
    svg.select(".y.axis").remove();
    svg.select(".x.axis").remove();
    svg
        .append("g")
        .attr("class", "y axis")
        .call(yAxis)
        .append("text")
        .attr("transform", "rotate(0)")
        .attr("x", 10)
        .attr("dx", ".1em")
        .style("text-anchor", "end")
        .text("");
    var bar = svg.selectAll(".bar").data(dataset, function (d) {
        return d.label;
    });
    // new data:
    bar
        .enter()
        .append("rect")
        .attr("class", "bar")
        .attr("x", function (d) {
            return x(d.value);
        })
        .attr("y", function (d) {
            return y(d.label);
        })
        .attr("width", function (d) {
            return width - x(d.value);
        })
        .attr("height", y.rangeBand());
    bar.on("mousemove", function (d) {
        div.style("left", d3.event.pageX + 10 + "px");
        div.style("top", d3.event.pageY - 25 + "px");
        div.style("display", "inline-block");
        div.html(d.label + "<br>" + d.value + "");
    });
    bar.on("mouseout", function (d) {
        div.style("display", "none");
    });
    // removed data:
    bar.exit().remove();
    // updated data:
    bar
        .transition()
        .duration(500)
        .attr("x", function (d) {
            return 0;
        })
        .attr("y", function (d) {
            return y(d.label);
        })
        .attr("width", function (d) {
            return x(d.value);
        })
        .attr("height", y.rangeBand()).style('fill', function (d, i) {
            var colors = ['#66D38C', ' #021434', '#CCF0D9', '#0B421D', '#177036'];
            return colors[i];
        });
}